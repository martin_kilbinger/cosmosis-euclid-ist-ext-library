#Code by Donnacha Kirk
#Edited by Simon Samuroff 07/2015 

# Martin Kilbinger:
# - 2016: new parameter zmin
# - 11/2016: no call of *gaussian* for sigmaz=0
# - 11/2016: removed unused parameter ngal
# - 11/2016: Combined photometric redshift error parameters
#	     into class
# - 11/2016: Added second Gaussian for catastrophic outliers
# - 11/2016: Additional (optional) photo-z parameters c_cal, f_out,
#            sigz_out, bias_out, c_cal_out

import numpy as np
from cosmosis.datablock import names as section_names
from cosmosis.datablock import option_section

class param:
    """General class to store (default) variables.
       Here used for photo-z error parameters
    """

    def __init__(self, **kwds):
        self.__dict__.update(kwds)

    def var_list(self, **kwds):
        return vars(self)


def gaussian(z,mu,sigma):
	return np.exp(-0.5*(z-mu)**2/sigma**2) / np.sqrt(2*np.pi) / sigma


def check_sigma(sigma, dz):
	"""Check and exit with error if sigma is numerically very close to
	   zero, leading to problems with gaussian photo-z convolution.
	"""

	if sigma < dz and sigma > 0:
		raise ValueError('Sigma of photometric error {} is greater than 0 but smaller than dz={}. This causes numerical ' \
				 'issues with convolution with gaussian.'.format(sigma, dz))


def smail_distribution(z, alpha, beta, z0):
	return (z**alpha) * np.exp(-(z/z0)**beta)

def photometric_error(z, Nz, par_phz):
	"""Return NzxNz matrix prob(ztrue=z, zphot) * n(zphot).

	Parameters
	----------
	z: array of double
		true redshifts
	Nz: array of double
		true redshift distribution n(z)
	par_phz: class param
		photo_z error parameters

	Returns
	-------
	output: Nz x Nz matrix of double
		probability matrix, output[i, j] = prob(zphot_i, ztrue_j) ntrue(ztrue_i)
	"""

	nz = len(z)
	output = np.zeros((nz,nz))

	# Loop over true redshifts z[i]
	for i in xrange(nz):
		# MK new 11/2016: else clause for no photo-z errors for sigma=0,
        	# caused problem with zmin>0
		# TODO: Deal with sigma_z_out = 0!
		if par_phz.sigma_z > 0:
			# MK new 11/2016: add second Gaussian for catastrophic outliers
                        # MKDEBUG 17/10/2017: z[i] -> z for dispersion, use true and not photo-z
			p = (1 - par_phz.f_out) * gaussian(z[i], par_phz.c_cal * z - par_phz.bias, par_phz.sigma_z*(1 + z[i])) \
			    + par_phz.f_out * gaussian(z[i], par_phz.c_cal_out * z - par_phz.bias_out, par_phz.sigma_z_out*(1 + z[i]))

                        # MKDEBUG: 19/10/2017, changed back to original matrix order!
			output[:,i] = p * Nz[i]

		else:
			# No photoz error: prob(z_phot|z_true) = delta(z_phot - z_true)
			output[i,i] = Nz[i]

	return output

def find_bins(z, nz_true, nbin):
	nz_true = nz_true/nz_true.sum()*nbin
	cum = np.cumsum(nz_true)
	bin_edges = [0.0]
	for i in xrange(1,nbin):
		edge = np.interp(1.0*i,cum,z)
		bin_edges.append(edge)
	bin_edges.append(z.max())	
	return np.array(bin_edges)

def find_bins_ph(z, nz_true, nbin, zmin, zmax):
	nz_true = nz_true/nz_true.sum()*nbin
	cum = np.cumsum(nz_true)
	bin_edges = [zmin]
	for i in xrange(1,nbin):
		edge = np.interp(1.0*i,cum,z)
		bin_edges.append(edge)
	bin_edges.append(zmax)	
	return np.array(bin_edges)


def compute_bin_nz(z_prob_matrix, z, edges):
        #print('compute_bin_nz, might take long if dz too small')
	NI = []
	nbin = len(edges)-1
	dz = z[1]-z[0]
	for low,high in zip(edges[:-1], edges[1:]):
		w = np.where((z>low) & (z<high))[0]
                # Sum over all possible zphot
		# Equivalent to marginalising p(zphot|ztrue) wrt ztrue
	        ni = z_prob_matrix[w,:].sum(axis=0)

		# Normalise the n(z) in each redshift bin to 1 over the redshift range
		# of the survey
		ni *= 1.0/(ni.sum()*dz)
		assert(len(ni)==len(z))
		NI.append(ni)
	return NI
	

def compute_nz(alpha, beta, z0, z, nbin, par_phz, zmin, zmax, z_bins_fixed=[]):
	#Set up Smail distribution of z vector as the distribution of true redshifts of the galaxies, n(ztrue)
	# MKDEBUG new: Parameters zmin, zmax

	nz_true = smail_distribution(z, alpha, beta, z0)

        # MKDEBUG 19/10/2017, removed again setting to 0 outside range, this was wrong

	# MKDEBUG new: Optional set bins in config file
	if len(z_bins_fixed) != 0:
		edges = z_bins_fixed
	else:
                # MKDEBUG: New function to find bins, look between zmin and zmax instead of 0 and zmaxmax
		#edges = find_bins(z,nz_true,nbin)
		edges = find_bins_ph(z,nz_true,nbin, zmin, zmax)

	# Multiply that by a Gaussian to get the probability distribution of the measured photo-z for each true redshift p(zphot|ztrue)
	# This gives a 2D probability distribution
	z_prob_matrix = photometric_error(z, nz_true, par_phz)
	bin_nz = compute_bin_nz(z_prob_matrix, z, edges)
	return edges,bin_nz


def setup(options):
	dz = options.get_double(option_section, "dz", default=0.01)
	# MK new: zmin, zmaxmax
	zmin = options.get_double(option_section, "zmin", default=0.0)
	zmax = options.get_double(option_section, "zmax", default=4.0)          # Max of photo-z selection
        zmaxmax = options.get_double(option_section, "zmaxmax", default=zmax)   # Max of true z, choose > zmax for photo-z errors
	nbin = options.get_int(option_section, "nbin")

	# MK new: fixed bin boundaries
        if options.has_value(option_section, 'z_bins_fixed'):
        	z_bins_fixed = options.get_double_array_1d(option_section, 'z_bins_fixed')
		if len(z_bins_fixed) != nbin + 1:
			raise ValueError('Number of fixed bin boundaries {} inconsistent with nbin={}'.format(len(z_bins_fixed), nbin))
	else:
		z_bins_fixed = []

	return (dz, zmin, zmax, zmaxmax, nbin, z_bins_fixed)

def execute(block, config):
	(dz, zmin, zmax, zmaxmax, nbin, z_bins_fixed) = config
	#read fits file data
	params = section_names.number_density_params
	#import pdb ; pdb.set_trace()
	alpha = block[params, "alpha"]
	beta = block[params, "beta"]
	z0 = block[params, "z0"]

	# Photo-z error parameters
	sigma_z = block[params, "sigz"]
	bias = block.get(params, "bias")

	# MK new 28/11. Additional optional parameters

	# c_cal: Calibration of photometric redshift, default=1 (perfect calibration)
	if block.has_value(params, 'c_cal'):
		c_cal = block.get(params, 'c_cal')
	else:
		c_cal = 1.0

	# f_out: Catastrophic outlier fraction, 0<=f_out<1, default=0 (no outliers)
	if block.has_value(params, 'f_out'):
       		f_out = block.get(params, 'f_out')
 	else:
       		f_out = 0
 	if f_out<0 or f_out>=1:
       		raise ValueError('Invalid catastrophic outlier fraction f_out={}, has to 0 <= f_out < 1'.format(f_out))

    	# sigma_z_out: Dispersion of outliers, default=sigma_z (same as regular photo-z's)
    	if block.has_value(params, 'sigz_out'):
        	sigma_z_out = block.get(params, 'sigz_out')
    	else:
        	sigma_z_out = sigma_z

    	# bias_out: Bias of outliers, default=0 (no bias)
    	if block.has_value(params, 'bias_out'):
        	bias_out = block.get(params, 'bias_out')
    	else:
        	bias_out = 0.0

    	# c_cal_out: Calibration of photometric redshifts of outliers, default=1
    	if block.has_value(params, "c_cal_out"):
        	c_cal_out = block.get(params, 'c_cal_out')
    	else:
        	c_cal_out = 1.0

	check_sigma(sigma_z, dz)
	check_sigma(sigma_z_out, dz)

	par_phz = param(
		sigma_z     = sigma_z,
		bias        = bias,
		c_cal       = c_cal,
		f_out       = f_out,
		sigma_z_out = sigma_z_out,
		bias_out    = bias_out,
		c_cal_out   = c_cal_out,
	)
	
	#Compute the redshift vector
	#z = np.arange(0.0,zmax+dz/2,dz)
        print('MKDEBUG: Extending (true) z array to zmaxmax={}'.format(zmaxmax))
	z = np.arange(0.0,zmaxmax+dz/2,dz)

	#Run the main code for getting n(z) in bins
	edges,bins = compute_nz(alpha, beta, z0, z, nbin, par_phz, zmin, zmax, z_bins_fixed=z_bins_fixed)

	#Save the results
	nz_section = section_names.wl_number_density
	block[nz_section,"nbin"] = nbin
	block[nz_section,"nz"] = len(z)
	block[nz_section,"z"] = z

	#Loop through the bins
	for i,bin in enumerate(bins):
		#The bin numbering starts at 1
		b=i+1
		name = "BIN_%d" % b
		#Save the bin edges as parameters
		block[nz_section,"EDGE_%d"%b] = edges[i]
		#And save the bin n(z) as a column
		block[nz_section, name] =  bin
	#Also save the upper limit to the top bin
	block[nz_section,"EDGE_%d"%(nbin+1)] = edges[-1]

	return 0
		

def cleanup(config):
	#nothing to do here!  We just include this 
	# for completeness
	return 0
	

	# for completeness
	return 0
	

	# for completeness
	return 0
	

