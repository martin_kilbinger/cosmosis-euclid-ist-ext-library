Welcome to CosmoSIS.  Please see our wiki for documentation:

https://bitbucket.org/joezuntz/cosmosis/wiki

This is the Euclid - IST (Inter-Science Taskforce: forecasts) fork.
It is a fork from the CosmoSIS standard library, a collection of standard cosmology modules for CosmoSIS, the COSMOlogy Survey Inference System.
For more details see the main repository at:
https://bitbucket.org/joezuntz/cosmosis
